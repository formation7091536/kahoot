import { Provider } from "react-redux";
import {
  createBrowserRouter,
  Navigate,
  RouterProvider,
} from "react-router-dom";

import "./App.scss";
import Login from "./pages/auth/login";
import Signup from "./pages/auth/signup";
import NotFound from "./pages/NotFound";
import Quiz from "./pages/quiz";

import Dashboard from "./pages/dashboard/Dashboard";
import AuthGuard from "./shared/guards/AuthGuard";
import GuestGuard from "./shared/guards/GuestGuard";
import GuestLayout from "./shared/layouts/GuestLayout";
import MainLayout from "./shared/layouts/MainLayout";
import store from "./shared/store";

const routes = [
  {
    path: "/",
    element: <Navigate to="/login" replace />,
  },
  {
    path: "/login",
    element: (
      <GuestGuard>
        <GuestLayout>
          <Login />
        </GuestLayout>
      </GuestGuard>
    ),
  },
  {
    path: "/signup",
    element: (
      <GuestGuard>
        <GuestLayout>
          <Signup />
        </GuestLayout>
      </GuestGuard>
    ),
  },
  {
    path: "/dashboard",
    element: (
      <AuthGuard>
        <MainLayout>
          <Dashboard />
        </MainLayout>
      </AuthGuard>
    ),
  },
  {
    path: "/quiz",
    element: (
      <AuthGuard>
        <MainLayout>
          <Quiz />
        </MainLayout>
      </AuthGuard>
    ),
  },
  {
    path: "*",
    element: <NotFound />,
  },
];

const router = createBrowserRouter(routes);

function App() {
  return (
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  );
}

export default App;
