import { combineReducers } from "@reduxjs/toolkit";
import authProvider from "../../pages/auth/data/authSlice";
import quizProvider from "../../pages/quiz/data/quizSlice";

const rootReducer = combineReducers({
  auth: authProvider,
  quiz: quizProvider,
});

export default rootReducer;
