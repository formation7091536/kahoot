import "./index.scss";
import { Props } from "./types";

export default function MainLayout({ children }: Readonly<Props>) {
  return <>{children}</>;
}
