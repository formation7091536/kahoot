import "./index.scss";
import { Props } from "./types";

export default function GuestLayout({ children }: Readonly<Props>) {
  return <>{children}</>;
}
