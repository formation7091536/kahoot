import { Navigate } from "react-router-dom";
import { useAppSelector } from "../store";

interface Props {
  children: React.ReactNode;
}

const GuestGuard = ({ children }: Props) => {
  const isAuthenticated = useAppSelector((state) => state.auth.isAuthenticated);
  return isAuthenticated ? <Navigate to="/quiz" /> : children;
};

export default GuestGuard;
