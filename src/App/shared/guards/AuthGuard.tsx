import { Navigate } from "react-router-dom";
import { useAppSelector } from "../store";

interface Props {
  children: React.ReactElement;
}

const AuthGuard = ({ children }: Props) => {
  const isAuthenticated = useAppSelector((store) => store.auth.isAuthenticated);
  return isAuthenticated ? children : <Navigate to="/login" />;
};

export default AuthGuard;
