import { createClient } from "@supabase/supabase-js";

const HOST_API = import.meta.env.VITE_APP_BASE_URL;
const API_KEY = import.meta.env.VITE_APP_API_KEY;
const supabase = createClient(HOST_API, API_KEY);

export default supabase;
