class Err extends Error {
  constructor(name?: string, message?: string) {
    super(message);
    this.name = name ?? "Error";
    this.stack = undefined;
    Object.setPrototypeOf(this, Err.prototype);
  }
}

export default Err;
