import axios from "axios";
// config
import { clearTokens, getTokens, setTokens } from "./token";

// ----------------------------------------------------------------------

const HOST_API = import.meta.env.VITE_APP_BASE_URL;
const API_KEY = import.meta.env.VITE_APP_API_KEY;

const headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
};

export const publicInstance = axios.create({ baseURL: HOST_API, headers });
export const axiosInstance = axios.create({ baseURL: HOST_API, headers });

publicInstance.interceptors.request.use(
  (config) => {
    config.headers["apiKey"] = API_KEY;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

axiosInstance.interceptors.request.use(
  (config) => {
    const { access_token } = getTokens();
    if (access_token) {
      config.headers["Authorization"] = `Bearer ${access_token}`;
      config.headers["apiKey"] = API_KEY;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

axiosInstance.interceptors.response.use(
  (response) => response,
  async (error) => {
    const previousRequest = error?.config;
    if (error?.response?.status === 401 && !previousRequest?.sent) {
      previousRequest.sent = true;
      try {
        const { refresh_token } = getTokens();
        const response = await axios.post(HOST_API + "/user", {
          refresh: refresh_token,
        });
        const { access, refresh } = response.data.data;
        setTokens(access, refresh);
        previousRequest.headers["Authorization"] = `Bearer ${access}`;
        return axiosInstance(previousRequest);
      } catch (err) {
        clearTokens();
      }
    }
    return Promise.reject(
      error?.response || error?.response?.data || "Something went wrong!",
    );
  },
);
