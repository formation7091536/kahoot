import "./index.scss";

export default function Logo() {
  return (
    <img
      className="logo"
      src="/assets/linkedin_banner_image_2.png"
      alt="Softyhoot logo"
    />
  );
}
