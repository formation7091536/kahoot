import Icon from "../Icon/Icon";
import "./index.scss";
import { Props } from "./types";

export default function Button({
  icon = "",
  label = "click",
  color = "primary",
  variant = "contained",
  size = "medium",
  hasShadow = false,
  className = "",
  onClick = () => {},
  disabled = false,
  sx = {},
}: Readonly<Props>) {
  return (
    <button
      className={`
        ${color}
        ${variant}
        ${size} 
        ${hasShadow && (color === "primary" ? "shadow-primary" : "shadow-secondary")} 
        ${disabled && "disabled"} 
        ${className}
      `}
      onClick={onClick}
      style={sx}
      disabled={disabled}
    >
      <span>
        {icon && <Icon icon={icon} />} {label}
      </span>
    </button>
  );
}
