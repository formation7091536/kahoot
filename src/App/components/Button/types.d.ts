export interface Props {
  icon?: React.RFC | string;
  label?: string;
  color?: "primary" | "secondary" | "transparent";
  variant?: "contained" | "outlined";
  size?: "small" | "medium" | "large";
  hasShadow?: boolean;
  className?: string;
  onClick?: () => void;
  disabled?: boolean;
  sx?: React.CSSProperties;
}
