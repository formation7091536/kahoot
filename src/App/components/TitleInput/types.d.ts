export interface Props {
  title: string;
  setTitle: (title: string) => void;
}
