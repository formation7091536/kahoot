/* eslint-disable @typescript-eslint/no-unused-vars */

import "./index.scss";
import { Props } from "./types";

export default function TitleInput({ title, setTitle }: Props) {
  function handleTitleChange(title: string) {
    setTitle(title);
  }

  return (
    <div className="softyhoot-title">
      <input
        type="text"
        placeholder="Enter softyhoot title..."
        value={title}
        onChange={(e) => handleTitleChange(e.target.value)}
      />
      <div className="label">settings</div>
    </div>
  );
}
