export type ProviderProps = {
  icon: string | null;
  name: string;
} & HTMLAttributes &
  HTMLDivElement &
  WithKeyProps;
