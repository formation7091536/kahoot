import "./index.scss";
import { ProviderProps } from "./types";

const providers = [
  {
    icon: "/assets/logos/google-99694fa3.svg",
    name: "Google",
  },
  {
    icon: "/assets/logos/microsoft-db0b4b54.svg",
    name: "Microsoft",
  },
  {
    icon: "/assets/logos/apple-ccbc762a.svg",
    name: "Apple",
  },
  {
    icon: "/assets/logos/clever-fad3ba8d.svg",
    name: "Clever",
  },
  {
    icon: null,
    name: "Single Sign-on",
  },
];

/**
 * Renders the social authentication providers.
 *
 * @returns a JSX element containing the social authentication providers.
 */
export default function SocialAuth() {
  return (
    <div className="providers-stack">
      {providers.map((provider) => (
        <Provider
          key={provider.name}
          icon={provider.icon}
          name={provider.name}
        />
      ))}
    </div>
  );
}

/**
 * @param icon - the icon to display for the provider
 * @param name - the name of the provider
 * @returns a JSX element representing the social auth provider
 */
function Provider({ icon, name }: ProviderProps) {
  return (
    <div className="provider">
      {icon ? <img src={icon} alt={name} /> : <div></div>}
      <span>continue with {name}</span>
      <div></div>
    </div>
  );
}
