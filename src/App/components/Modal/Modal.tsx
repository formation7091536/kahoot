import React, { useState } from "react";
import "./Modal.scss";

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  children: React.ReactNode;
  className?: string;
}

const Modal: React.FC<ModalProps> = ({
  isOpen,
  onClose,
  children,
  className,
}) => {
  const [isAnimating, setIsAnimating] = useState(false);

  const closeModal = () => {
    setIsAnimating(true);
    setTimeout(() => {
      onClose();
      setIsAnimating(false);
    }, 300);
  };

  return (
    <div
      className={`modal ${isOpen ? "open" : ""} ${isAnimating ? "closing" : ""}`}
    >
      <div className="modal-overlay" onClick={closeModal}></div>
      <div className={`modal-content ${className}`}>{children}</div>
    </div>
  );
};

export default Modal;
