import { Props } from "./types";

export default function IconWrapper({ children }: Readonly<Props>) {
  return <div className="icon-wrapper">{children}</div>;
}
