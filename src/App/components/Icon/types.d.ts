export interface Props {
  icon: React.RFC | string;
  size?: number;
  cursor?: string;
  className?: string;
  Wrapper?: React.RFC;
  onClick?: () => void;
  disabled?: boolean;
}
