import { Fragment } from "react";
import "./index.scss";
import { Props } from "./types";

export default function Icon({
  icon,
  size = 24,
  cursor = "default",
  className,
  Wrapper = Fragment,
  onClick = () => {},
  disabled = false,
}: Readonly<Props>) {
  const styleBtnObj = { cursor: disabled ? "not-allowed" : cursor };
  const styleImgObj = { width: `${size}px`, height: `${size}px` };

  if (typeof icon !== "string")
    return (
      <button
        onClick={onClick}
        className={`icon ${className}`}
        style={styleBtnObj}
        disabled={disabled}
      >
        {icon}
      </button>
    );

  return icon.length <= 3 ? (
    <button
      onClick={onClick}
      className={`icon ${className}`}
      style={styleBtnObj}
      disabled={disabled}
    >
      {icon}
    </button>
  ) : (
    <Wrapper>
      <button
        onClick={onClick}
        disabled={disabled}
        className={`icon ${className}`}
        style={styleBtnObj}
      >
        <img src={icon} alt="icon" style={styleImgObj} />
      </button>
    </Wrapper>
  );
}
