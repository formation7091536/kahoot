import { Link } from "react-router-dom";

export default function NotFound() {
  return (
    <>
      <h1 className="primary-heading">🥺 404 Page Not Found!</h1>
      <Link style={{ textDecoration: "underline" }} to="/">
        {" "}
        &larr; Go back to home
      </Link>
    </>
  );
}
