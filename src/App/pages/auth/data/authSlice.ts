/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { clearTokens, getTokens, setTokens } from "../../../shared/utils/token";
import { login, logout, me, register } from "./authThunk";
import { User } from "./authTypes";

export interface AuthState {
  status: string;
  isAuthenticated: boolean;
  isInitialised: boolean;
  user: User | null;
  error: string | null;
}

const initialState: AuthState = {
  status: "idle",
  isAuthenticated: !!getTokens().access_token,
  isInitialised: false,
  user: null,
  error: null,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    initialise: (state, action) => {
      const { user } = action.payload;
      state.isAuthenticated = !!getTokens().access_token;
      state.isInitialised = true;
      state.user = { id: user.id, email: user.email };
    },
    restore: (state) => {
      state.error = null;
      state.status = "idle";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(login.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(login.fulfilled, (state, action) => {
        console.log(action);
        const { access_token, refresh_token, user } = action.payload;
        console.log(action.payload);
        setTokens(access_token, refresh_token);
        state.isAuthenticated = true;
        state.user = { id: user.id, email: user.email };
        state.status = "succeeded";
      })
      .addCase(login.rejected, (state, action) => {
        console.log(action);
        state.error =
          action?.error?.message?.slice(-1, 3) === "400"
            ? "Invalid credentials"
            : "Unknown error";
        state.status = "failed";
      })
      .addCase(logout.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(logout.fulfilled, (state) => {
        state.isAuthenticated = false;
        state.user = null;
        state.status = "succeeded";
        clearTokens();
      })
      .addCase(logout.rejected, (state, action: PayloadAction<any>) => {
        state.error = action?.payload;
        state.status = "failed";
      })

      .addCase(register.pending, (state) => {
        state.error = null;
        state.status = "laoding";
      })
      .addCase(register.fulfilled, (state, action) => {
        state.user = action.payload.user;
        state.status = "succeeded";
      })
      .addCase(register.rejected, (state, action) => {
        state.error = action.error.message ?? "Unknow error!";
        state.status = "failed";
      })
      .addCase(me.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(me.fulfilled, (state, action) => {
        console.log(action);
        const { id, email } = action.payload;
        state.user = { id, email };
        state.status = "succeeded";
      })
      .addCase(me.rejected, (state, action) => {
        state.error = action.error.message ?? "Unkown error!";
        state.status = "failed";
      });
  },
});

export const { initialise, restore } = authSlice.actions;

export default authSlice.reducer;
