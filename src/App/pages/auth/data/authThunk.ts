/* eslint-disable @typescript-eslint/no-explicit-any */
import { createAsyncThunk } from "@reduxjs/toolkit";
import { axiosInstance, publicInstance } from "../../../shared/utils/axios";
import { LoginPayload, RegisterPayload } from "./authTypes";

export const login = createAsyncThunk(
  "auth/login",
  async (query: LoginPayload) => {
    try {
      const response = await publicInstance.post(
        `/auth/v1/token?grant_type=password`,
        query,
      );

      if (response.status === 200) {
        console.log(response);
        return response.data;
      }

      throw new Error(response.statusText);
    } catch (err: any) {
      return Promise.reject(err.message);
    }
  },
);

export const register = createAsyncThunk(
  "auth/register",
  async (query: RegisterPayload) => {
    try {
      const response = await publicInstance.post(`/auth/v1/signup`, query);

      if (response.status === 200) {
        return response.data;
      }

      throw new Error(response.statusText);
    } catch (err: any) {
      return Promise.reject(err.message);
    }
  },
);

export const me = createAsyncThunk("auth/me", async () => {
  try {
    const response = await axiosInstance.get(`/auth/v1/user`);
    if (response.status === 200) {
      return response.data;
    }

    throw new Error(response.statusText);
  } catch (err: any) {
    return Promise.reject(err.message);
  }
});

export const logout = createAsyncThunk("auth/logout", async () => {
  try {
    const response = await axiosInstance.post(`/auth/v1/logout`);

    if (response.status === 204) {
      return response.data;
    }

    throw new Error(response.statusText);
  } catch (err: any) {
    return Promise.reject(err.message);
  }
});
