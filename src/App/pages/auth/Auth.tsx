import { ChangeEvent, FormEvent, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import SocialAuth from "../../components/SocialAuth";
import { useAppDispatch, useAppSelector } from "../../shared/store";
import { restore } from "./data/authSlice";
import { login, register } from "./data/authThunk";
import "./index.scss";

export default function Auth({ type }: Readonly<{ type: string }>) {
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useAppDispatch();
  const { status, isAuthenticated } = useAppSelector((store) => store.auth);
  const navigate = useNavigate();

  useEffect(() => {
    if (isAuthenticated) navigate("/dashboard");
  }, [isAuthenticated, navigate]);

  function handleUserChange(e: ChangeEvent<HTMLInputElement>) {
    setUser(e.target.value);
    status === "failed" && dispatch(restore());
  }

  function handlePasswordChange(e: ChangeEvent<HTMLInputElement>) {
    setPassword(e.target.value);
    status === "failed" && dispatch(restore());
  }

  async function authHandler(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (type === "log in") {
      await dispatch(login({ email: user, password }));
      status === "succeeded" && navigate("/dashboard");
    } else if (type === "sign up")
      await dispatch(register({ email: user, password }));
    else return;
  }

  return (
    <div className="auth">
      {/* @TO-DO */}
      {/* <Header/> */}
      <header>
        <h1 className="primary-heading">{type}</h1>
      </header>
      <form className="form" onSubmit={authHandler}>
        <div className="field">
          <label htmlFor="username">username or email</label>
          <input
            type="text"
            id="username"
            onChange={handleUserChange}
            value={user}
          />{" "}
        </div>

        <div className="field">
          <label htmlFor="password">password</label>
          <input
            type="password"
            id="password"
            onChange={handlePasswordChange}
            value={password}
          />
        </div>
        <footer className="form-footer">
          {type === "log in" && (
            <p className="forget">
              forget password ?{" "}
              <Link to="/reset" className="reset">
                Reset your password
              </Link>
            </p>
          )}
          <button
            className={`submit ${user && password && status !== "loading" && status !== "failed" ? "active" : ""}`}
            disabled={
              status === "failed" ||
              status === "loading" ||
              (status === "idle" && (!user || !password))
            }
          >
            {status === "loading" ? "loading..." : type}
          </button>
          <span className="error-message">
            {status === "failed" && "Invalid credentials!"}
          </span>
        </footer>
      </form>

      <div className="seperator">
        <hr className="line" />
        <span className="flag">or</span>
      </div>

      <SocialAuth />

      <footer>
        <p className="footer">
          {type === "log in" ? (
            <>
              don't have an account?{" "}
              <Link to="/signup" className="link">
                Sign up
              </Link>
            </>
          ) : (
            <>
              already have an account?{" "}
              <Link to="/login" className="link">
                Log in
              </Link>
            </>
          )}
        </p>
      </footer>
    </div>
  );
}
