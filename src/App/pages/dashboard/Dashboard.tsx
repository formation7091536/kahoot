import { useState } from "react";
import { Navigate } from "react-router-dom";
import { v4 as uuid4 } from "uuid";
import Button from "../../components/Button";
import Modal from "../../components/Modal";
import TitleInput from "../../components/TitleInput";
import { useAppDispatch, useAppSelector } from "../../shared/store";
import { me } from "../auth/data/authThunk";
import { createQuiz } from "../quiz/data/quizThunk";
import "./index.scss";

export default function Dashboard() {
  const dispatch = useAppDispatch();
  // const navigate = useNavigate();

  const user = useAppSelector((store) => store.auth.user);
  const status = useAppSelector((store) => store.quiz.status);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [quiz, setQuiz] = useState("");

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  if (!user) dispatch(me());

  function handleCreateSoftyhoot() {
    openModal();
  }

  function createSoftyhoot() {
    dispatch(
      createQuiz({
        id: uuid4(),
        title: quiz,
      }),
    );
  }

  return (
    <div className="dashboard">
      <Button
        color="secondary"
        label="See you previous Softyhoots"
        variant="outlined"
      />
      <Modal
        isOpen={isModalOpen}
        onClose={closeModal}
        className="create-softyhoot-form"
      >
        <h2>Create your Softyhoot</h2>
        {status === "loading" && <p>Loading...</p>}
        {status === "failed" && <p>Something went wrong...</p>}
        {status === "idle" && (
          <>
            <TitleInput title={quiz} setTitle={setQuiz} />
            <div className="create-softyhoot-actions">
              <Button
                label="cancel"
                color="secondary"
                variant="outlined"
                onClick={closeModal}
              />
              <Button label="create" onClick={createSoftyhoot} />
            </div>
          </>
        )}
        {status === "succeeded" && <Navigate to="/quiz" />}
      </Modal>
      <Button label="Create a new Softyhoot" onClick={handleCreateSoftyhoot} />
    </div>
  );
}
