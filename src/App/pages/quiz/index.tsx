import Quiz from "./Quiz";
import QuizGuard from "./utils/QuizGuard";

function ProtectedQuiz() {
  return (
    <QuizGuard>
      <Quiz />
    </QuizGuard>
  );
}

export default ProtectedQuiz;
