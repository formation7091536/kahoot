import { createAsyncThunk } from "@reduxjs/toolkit";
import { axiosInstance } from "../../../shared/utils/axios";
import Err from "../../../shared/utils/err";
import { AnswerPayload, QuestionPayload, Quiz, QuizPayload } from "./types";

export const createQuiz = createAsyncThunk(
  "quiz/create",
  async (query: QuizPayload) => {
    try {
      axiosInstance.interceptors.request.use(
        (config) => {
          config.headers["Prefer"] = "return=minimal";
          return config;
        },
        (error) => {
          return Promise.reject(error);
        },
      );

      const response = await axiosInstance.post(`/rest/v1/quiz`, query);

      if (response.status === 201) {
        console.log(response);
        return query;
      }

      if (response.status === 409) {
        console.log(response);
        return Promise.reject(
          new Error("Quiz with this title already exists."),
        );
      }

      throw new Error(response.statusText);
    } catch (err: unknown) {
      console.error(err);
      if (err instanceof Error) {
        return Promise.reject(err.message);
      }
      // Handle the case where the caught value is not an Error instance
      else if ((err as { status: number }).status === 409) {
        return Promise.reject(
          new Err("duplicate", "Quiz with this title already exists. (409)"),
        );
      } else if (
        (err as { data: { message: string } }).data &&
        typeof (err as { data: { message: string } }).data.message === "string"
      ) {
        return Promise.reject(
          new Error((err as { data: { message: string } }).data.message),
        );
      } else {
        return Promise.reject(new Error("Unknown error occurred."));
      }
    }
  },
);

export const getQuiz = createAsyncThunk("quiz/getQuiz", async (id: string) => {
  try {
    const response = await axiosInstance.get(`/rest/v1/quiz?select=*`);

    if (response.status === 200) {
      const data = response.data.filter((quiz: Quiz) => quiz.id === id)[0];
      return data;
    }
  } catch (error) {
    return Promise.reject(error);
  }
});

export const getQuizzes = createAsyncThunk("quiz/getQuizzes", async () => {
  try {
    const response = await axiosInstance.get(`/rest/v1/quiz?select=*`);

    if (response.status === 200) {
      return response.data;
    }
  } catch (error) {
    return Promise.reject(error);
  }
});

export const createQuestion = createAsyncThunk(
  "quiz/createQuestion",
  async (query: QuestionPayload) => {
    try {
      const response = await axiosInstance.post("/rest/v1/question", query);

      if (response.status === 201) {
        return query;
      }
    } catch (error) {
      return Promise.reject(error);
    }
  },
);

export const updateQuestion = createAsyncThunk(
  "quiz/updateQuestion",
  async (query: QuestionPayload) => {
    try {
      const response = await axiosInstance.patch(
        `/rest/v1/question?id=eq.${query.id}`,
        query,
      );

      if (response.status === 204) {
        return query;
      }
    } catch (error) {
      return Promise.reject(error);
    }
  },
);

export const deleteQuestion = createAsyncThunk(
  "quiz/deleteQuestion",
  async (id: string) => {
    try {
      const response = await axiosInstance.delete(
        `/rest/v1/question?id=eq.${id}`,
      );

      if (response.status === 204) {
        return { id };
      }
    } catch (error) {
      return Promise.reject(error);
    }
  },
);

export const createAnswer = createAsyncThunk(
  "quiz/createAnswer",
  async (query: AnswerPayload) => {
    try {
      const response = await axiosInstance.post("/rest/v1/option", query);

      if (response.status === 201) {
        return query;
      }
    } catch (error) {
      return Promise.reject(error);
    }
  },
);

export const updateAnswer = createAsyncThunk(
  "quiz/updateAnswer",
  async (query: AnswerPayload) => {
    try {
      const response = await axiosInstance.patch(
        `/rest/v1/option?id=eq.${query.id}`,
        query,
      );

      if (response.status === 204) {
        return query;
      }
    } catch (error) {
      return Promise.reject(error);
    }
  },
);
