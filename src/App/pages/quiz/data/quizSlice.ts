import { createSlice } from "@reduxjs/toolkit";
import { saveQuiz } from "../utils/savedQuiz";
import {
  createAnswer,
  createQuestion,
  createQuiz,
  deleteQuestion,
  getQuiz,
  updateAnswer,
  updateQuestion,
} from "./quizThunk";
import { Question, Quiz } from "./types";

export interface QuizState {
  status: string;
  isInitialised: boolean;
  quiz: Quiz | null;
  error: string | null;
}

const initialState: QuizState = {
  status: "idle",
  isInitialised: false,
  quiz: null,
  error: null,
};

const quizSlice = createSlice({
  name: "quiz",
  initialState,
  reducers: {
    initialise: (state, action) => {
      const { quiz } = action.payload;
      state.isInitialised = true;
      state.quiz = { id: quiz.id, title: quiz.title };
    },
    restore: (state) => {
      state.error = null;
      state.status = "idle";
    },
    selectQuestionById(state, action) {
      if (state?.quiz)
        state.quiz.questions = state.quiz?.questions?.map((q: Question) => {
          q.isSelected = q.id === action.payload.id;
          return q;
        });
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getQuiz.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(getQuiz.fulfilled, (state, action) => {
        state.quiz = action.payload;
        state.status = "succeeded";
      })
      .addCase(getQuiz.rejected, (state, action) => {
        state.error = action?.error?.message ?? "Unknown error";
        state.status = "failed";
      })
      .addCase(createQuiz.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(createQuiz.fulfilled, (state, action) => {
        console.log(action);
        const quiz = action.payload;
        console.log(action.payload);
        state.quiz = { id: quiz.id, title: quiz.title, questions: [] };
        saveQuiz(quiz.id);
        state.status = "succeeded";
      })
      .addCase(createQuiz.rejected, (state, action) => {
        console.log(action);
        state.error = action?.error?.message ?? "Unknown error";
        state.status = "failed";
      })
      .addCase(createQuestion.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(createQuestion.fulfilled, (state, action) => {
        console.log(action);
        state.status = "succeeded";
        if (state?.quiz)
          state.quiz.questions = state.quiz?.questions?.map((q: Question) => {
            return { ...q, isSelected: false };
          });
        const question = {
          id: action.payload?.id ?? "",
          title: action.payload?.title ?? "",
          type: action.payload?.type ?? "quiz",
          points: action.payload?.points ?? 1,
          timeLimit: action.payload?.timeLimit ?? 30,
          answers: [],
          isSelected: true,
        };
        state.quiz?.questions?.push(question);
      })
      .addCase(createQuestion.rejected, (state, action) => {
        console.log(action);
        state.error = action?.error?.message ?? "Unknown error";
        state.status = "failed";
      })
      .addCase(updateQuestion.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(updateQuestion.fulfilled, (state, action) => {
        if (state?.quiz)
          state.quiz.questions = state?.quiz?.questions?.map((q: Question) => {
            if (q.id === action.payload?.id) {
              return { ...q, ...action.payload };
            }
            return q;
          });
        state.status = "succeeded";
      })
      .addCase(updateQuestion.rejected, (state, action) => {
        state.error = action?.error?.message ?? "Unknown error";
        state.status = "failed";
      })
      .addCase(deleteQuestion.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(deleteQuestion.fulfilled, (state, action) => {
        if (state?.quiz)
          state.quiz.questions = state.quiz?.questions?.filter(
            (q: Question) => q.id !== action.payload?.id,
          );
        state.status = "succeeded";
      })
      .addCase(deleteQuestion.rejected, (state, action) => {
        state.error = action?.error?.message ?? "Unknown error";
        state.status = "failed";
      })
      .addCase(createAnswer.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(createAnswer.fulfilled, (state, action) => {
        if (state?.quiz)
          state.quiz.questions = state?.quiz?.questions?.map((q: Question) => {
            if (q.id === action.payload?.question) {
              const { id, title, photo, isPhoto, isCorrect } = action.payload;
              q.answers?.push({ id, title, photo, isPhoto, isCorrect });
            }
            return q;
          });
        state.status = "succeeded";
      })
      .addCase(createAnswer.rejected, (state, action) => {
        state.error = action?.error?.message ?? "Unknown error";
        state.status = "failed";
      })
      .addCase(updateAnswer.pending, (state) => {
        state.error = null;
        state.status = "loading";
      })
      .addCase(updateAnswer.fulfilled, (state, action) => {
        if (state?.quiz)
          state.quiz.questions = state?.quiz?.questions?.map((q: Question) => {
            if (q.id === action.payload?.question) {
              q.answers = q.answers?.map((a) => {
                if (a.id === action.payload?.id) {
                  return { ...a, ...action.payload };
                }
                return a;
              });
            }
            return q;
          });
        state.status = "succeeded";
      })
      .addCase(updateAnswer.rejected, (state, action) => {
        state.error = action?.error?.message ?? "Unknown error";
        state.status = "failed";
      });
  },
});

export const { initialise, restore, selectQuestionById } = quizSlice.actions;

export default quizSlice.reducer;
