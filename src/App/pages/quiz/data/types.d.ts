export interface Answer {
  id: string;
  title: string;
  isPhoto: boolean;
  photo?: string;
  isCorrect: boolean;
}

export interface Question {
  id: string;
  title: string;
  points: number;
  timeLimit: number;
  photo?: string;
  answers?: Answer[];
  isSelected: boolean;
}

export interface Quiz {
  id: string;
  title: string;
  points?: number;
  timeLimit?: number;
  questions?: Question[];
}

export interface AnswerPayload {
  id: string;
  title: string;
  isPhoto: boolean;
  photo?: string;
  question?: string;
  isCorrect: boolean;
}

export interface QuestionPayload {
  id: string;
  title?: string;
  quiz?: string;
  type?: string;
  points?: number;
  timeLimit?: number;
  photo?: string;
}

export interface QuizPayload {
  id: string;
  title: string;
}
