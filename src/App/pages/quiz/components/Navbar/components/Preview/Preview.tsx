import Icon from "../../../../../../components/Icon";
import IconWrapper from "../../../../../../components/IconWrapper";
import { useAppDispatch } from "../../../../../../shared/store";
import { deleteQuestion } from "../../../../data/quizThunk";
import "./index.scss";
import { Props } from "./types";

export default function Preview({
  id,
  number,
  title,
  isSelected,
  onClick,
}: Readonly<Props>) {
  const dispatch = useAppDispatch();

  console.log(id);

  function handleDelete() {
    dispatch(deleteQuestion(id));
  }

  return (
    <div
      className={`preview ${isSelected ? "selected" : ""}`}
      // draggable={true}
      onClick={onClick}
    >
      <div className="preview-actions">
        <Icon
          icon="../../../../../../../../assets/icons/copy-outline.svg"
          size={15}
          cursor="pointer"
          Wrapper={IconWrapper}
          disabled
        />
        <Icon
          icon="../../../../../../../../assets/icons/trash-outline.svg"
          cursor="pointer"
          size={15}
          Wrapper={IconWrapper}
          onClick={handleDelete}
        />
      </div>
      <div className="previewer-wrapper">
        <h3 className="previewer-title">
          {number} {title}
        </h3>
        <div className={`previewer-box ${isSelected ? "selected" : ""}`}>
          preview
        </div>
      </div>
    </div>
  );
}
