export interface Props {
  id: string;
  number: number;
  title: string;
  isSelected: boolean;
  onClick: () => void;
}
