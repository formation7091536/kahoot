import { v4 as uuid4 } from "uuid";
import Button from "../../../../components/Button";
import { useAppDispatch, useAppSelector } from "../../../../shared/store";
import { selectQuestionById } from "../../data/quizSlice";
import { createQuestion } from "./../../data/quizThunk";
import Preview from "./components/Preview";
import "./index.scss";

export default function Navbar() {
  const quiz = useAppSelector((store) => store.quiz.quiz);
  const questions = quiz?.questions ?? [];
  const dispatch = useAppDispatch();

  function handleAddQuestion() {
    dispatch(
      createQuestion({
        id: uuid4(),
        title: "",
        quiz: quiz?.id ?? "",
        type: "quiz",
        points: quiz?.points ?? 1,
        timeLimit: quiz?.timeLimit ?? 30,
      }),
    );
  }

  function selectQuestion(id: string) {
    dispatch(selectQuestionById({ id }));
  }

  return (
    <div className="navbar">
      <div className="previewer-container">
        {questions.map((question, index) => {
          return (
            <Preview
              key={question.id}
              id={question.id}
              number={index + 1}
              title={question.title}
              isSelected={question.isSelected}
              onClick={() => selectQuestion(question.id)}
            />
          );
        })}
      </div>
      <div className="actions">
        <Button
          label="add question"
          hasShadow={true}
          onClick={handleAddQuestion}
        />
        <Button color="secondary" label="add slide" hasShadow={true} disabled />
      </div>
    </div>
  );
}
