import { useAppSelector } from "../../../../shared/store";
import Select from "./components/Select";
import "./index.scss";

export default function OptionsDrawer() {
  const timeLimits = [
    {
      label: "5 seconds",
      value: 5,
    },
    {
      label: "10 seconds",
      value: 10,
    },
    {
      label: "20 seconds",
      value: 20,
    },
    {
      label: "30 seconds",
      value: 30,
    },
    {
      label: "1 minute",
      value: 60,
    },
    {
      label: "1 minute 30 seconds",
      value: 90,
    },
    {
      label: "2 minutes",
      value: 120,
    },
    {
      label: "3 minutes",
      value: 180,
    },
    {
      label: "4 minutes",
      value: 240,
    },
  ];

  const points = [
    {
      label: "Standard",
      value: 1,
    },
    {
      label: "Double points",
      value: 2,
    },
    {
      label: "No points",
      value: 0,
    },
  ];

  const questions = useAppSelector((store) => store.quiz.quiz?.questions ?? []);

  function onChange() {}

  return (
    questions.length > 0 && (
      <div className="options-drawer">
        Time Limit
        <Select
          options={timeLimits}
          onChange={onChange}
          defaultValue={timeLimits[3]}
        />
        Points
        <Select options={points} onChange={onChange} defaultValue={points[0]} />
      </div>
    )
  );
}
