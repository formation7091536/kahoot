import React, { useEffect, useRef, useState } from "react";
import "./index.scss";
import { CustomSelectProps, Option } from "./types";

// Icon component
const Icon = ({ isOpen }: { isOpen: boolean }) => {
  return (
    <svg
      viewBox="0 0 24 24"
      width="18"
      height="18"
      stroke="#222"
      strokeWidth="1.5"
      fill="none"
      strokeLinecap="round"
      strokeLinejoin="round"
      className={isOpen ? "translate" : ""}
    >
      <polyline points="6 9 12 15 18 9"></polyline>
    </svg>
  );
};

// CloseIcon component
const CloseIcon = () => {
  return (
    <svg
      viewBox="0 0 24 24"
      width="14"
      height="14"
      stroke="#fff"
      strokeWidth="2"
      fill="none"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <line x1="18" y1="6" x2="6" y2="18"></line>
      <line x1="6" y1="6" x2="18" y2="18"></line>
    </svg>
  );
};

// CustomSelect component
const CustomSelect = ({
  placeHolder = "Select",
  defaultValue,
  options,
  isMulti = false,
  isSearchable = false,
  onChange,
  align,
}: CustomSelectProps) => {
  // State variables using React hooks
  const [showMenu, setShowMenu] = useState(false); // Controls the visibility of the dropdown menu
  const initialSelectedValue: Option | Option[] | null = isMulti
    ? []
    : defaultValue ?? null;

  const [selectedValue, setSelectedValue] = useState(initialSelectedValue); // Stores the selected value(s)
  const [searchValue, setSearchValue] = useState(""); // Stores the value entered in the search input
  const searchRef = useRef<HTMLInputElement>(null); // Reference to the search input element
  const inputRef = useRef<HTMLButtonElement>(null); // Reference to the custom select input element

  useEffect(() => {
    setSearchValue("");
    if (showMenu && searchRef.current) {
      (searchRef.current as HTMLInputElement).focus();
    }
  }, [showMenu]);

  useEffect(() => {
    const handler = function (this: Window, e: Event) {
      if (
        inputRef.current &&
        !(inputRef.current as HTMLElement).contains(e.target as Node)
      ) {
        setShowMenu(false);
      }
    };

    window.addEventListener("click", handler);
    return () => {
      window.removeEventListener("click", handler);
    };
  });

  const handleInputClick = () => {
    setShowMenu(!showMenu);
  };

  const getDisplay = () => {
    if (
      !selectedValue ||
      (Array.isArray(selectedValue) && selectedValue.length === 0)
    ) {
      return placeHolder;
    }

    if (isMulti) {
      return (
        <div className="dropdown-tags">
          {Array.isArray(selectedValue) &&
            selectedValue.map((option: Option, index: number) => (
              <div
                key={`${option.value}-${index}`}
                className="dropdown-tag-item"
              >
                {option.label}
                <span
                  onClick={() => onTagRemove(option)}
                  className="dropdown-tag-close"
                >
                  <CloseIcon />
                </span>
              </div>
            ))}
        </div>
      );
    }

    return !Array.isArray(selectedValue) ? selectedValue.label : placeHolder;
  };

  const removeOption = (option: Option) => {
    return Array.isArray(selectedValue)
      ? selectedValue?.filter((o: Option) => o.value !== option.value)
      : [];
  };

  const onTagRemove = (option: Option) => {
    const newValue = removeOption(option);
    if (newValue) {
      setSelectedValue(newValue);
      onChange(newValue);
    }
  };

  const onItemClick = (option: Option) => {
    let newValue: Option | Option[] | null;
    if (isMulti) {
      if (
        Array.isArray(selectedValue) &&
        selectedValue.findIndex((o: Option) => o.value === option.value) >= 0
      ) {
        newValue = removeOption(option);
      } else {
        newValue = Array.isArray(selectedValue)
          ? [...selectedValue, option]
          : option;
      }
    } else {
      newValue = option;
    }
    setSelectedValue(newValue);
    onChange(newValue);
  };

  const isSelected = (option: Option) => {
    if (isMulti && Array.isArray(selectedValue)) {
      return (
        selectedValue.filter((o: Option) => o.value === option.value).length > 0
      );
    }

    if (!selectedValue) {
      return false;
    }

    return (
      !Array.isArray(selectedValue) && selectedValue.value === option.value
    );
  };

  const onSearch = (e: React.FormEvent<HTMLInputElement>) => {
    setSearchValue((e.target as HTMLInputElement).value);
  };

  const getOptions = () => {
    if (!searchValue) {
      return options;
    }

    return options.filter(
      (option) =>
        option.label.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0,
    );
  };

  return (
    <div className="custom--dropdown-container">
      <button
        ref={inputRef}
        onClick={handleInputClick}
        className="dropdown-input"
      >
        <div
          className={`dropdown-selected-value ${!selectedValue || (Array.isArray(selectedValue) && selectedValue.length === 0) ? "placeholder" : ""}`}
        >
          {getDisplay()}
        </div>
        <div className="dropdown-tools">
          <div className="dropdown-tool">
            <Icon isOpen={showMenu} />
          </div>
        </div>
      </button>

      {showMenu && (
        <div className={`dropdown-menu alignment--${align ?? "auto"}`}>
          {isSearchable && (
            <div className="search-box">
              <input
                className="form-control"
                onChange={onSearch}
                value={searchValue}
                ref={searchRef}
              />
            </div>
          )}
          {getOptions().map((option) => (
            <div
              onClick={() => onItemClick(option)}
              key={option.value}
              className={`dropdown-item ${isSelected(option) && "selected"}`}
            >
              {option.label}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default CustomSelect;
