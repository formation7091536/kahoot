interface Option {
  label: string;
  value: number;
}

export interface CustomSelectProps {
  placeHolder?: string;
  defaultValue?: Option;
  options: Option[];
  isMulti?: boolean;
  isSearchable?: boolean;
  onChange: (value: Option[] | Option | null) => void;
  align?: string;
}
