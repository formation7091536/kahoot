import { useState } from "react";
import { useNavigate } from "react-router-dom";
import Button from "../../../../components/Button";
import Logo from "../../../../components/Logo";
import TitleInput from "../../../../components/TitleInput";
import { useAppDispatch, useAppSelector } from "../../../../shared/store";
import { logout } from "../../../auth/data/authThunk";
import "./index.scss";

export default function Header() {
  const dispatch = useAppDispatch();
  const status = useAppSelector((store) => store.auth.status);
  const quiz = useAppSelector((store) => store.quiz.quiz);
  const [title, setTitle] = useState(quiz?.title ?? "");
  const navigate = useNavigate();

  async function handleLogout() {
    await dispatch(logout());
    status === "succeeded" && navigate("/login");
  }

  return (
    <header className="header">
      <div className="subheader--0">
        <Logo />
        <TitleInput title={title} setTitle={setTitle} />
      </div>
      <div className="subheader--1">
        <Button icon="🎨" label="themes" />
        <Button icon="👁️" label="preview" color="secondary" />
        <div className="seperator"></div>
        <Button label="exit" color="secondary" onClick={handleLogout} />
        <Button
          label="save"
          color="secondary"
          className="secondary--0"
          disabled={true}
        />
      </div>
    </header>
  );
}
