import { useAppDispatch } from "../../../../../../shared/store";
import { updateQuestion } from "../../../../data/quizThunk";
import { Question } from "../../../../data/types";
import Input from "../Input";
import "./index.scss";

export default function QuestionInput({
  question,
}: Readonly<{ question: Question }>) {
  const dispatch = useAppDispatch();

  function handleEdit(title: string) {
    dispatch(updateQuestion({ id: question.id, title }));
  }

  return (
    <Input
      value={question.title ?? ""}
      placeholder="Start typing your question"
      onChange={handleEdit}
      max={140}
      inputClassName="question-input"
      hintDefault="chars-hint"
      hintDanger="danger-hint"
    />
  );
}
