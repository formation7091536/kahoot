import _debounce from "lodash/debounce";
import { ChangeEvent, useCallback, useEffect, useState } from "react";
import Button from "../../../../../../components/Button";
import { Props } from "./types";

export default function Input({
  index,
  value = "",
  placeholder = "",
  onChange = (value: string | { value: string; isCorrect: boolean }) => value,
  max = Infinity,
  inputClassName = "",
  hintDefault = "",
  hintDanger = "",
  isAnswer = false,
  hasSaveButton = false,
}: Readonly<Props>) {
  const [currentValue, setCurrentValue] = useState(value);
  const [isSaved, setIsSaved] = useState(false);
  const [isCorrect, setIsCorrect] = useState(false);

  const debounceFn = useCallback(
    _debounce((inputValue: string) => onChange(inputValue), 1500),
    [onChange, value],
  );

  const debounceFnUpdateAnswer = useCallback(
    _debounce(({ index, value, isCorrect }) => {
      onChange({ index, value, isCorrect });
    }, 1500),
    [onChange],
  );

  function handleEdit(event: ChangeEvent<HTMLInputElement>) {
    if (event.target.value.length <= max) {
      setCurrentValue(event.target.value);
      setIsSaved(false);
      !isAnswer
        ? debounceFn(event.target.value)
        : debounceFnUpdateAnswer({
            index,
            value: event.target.value,
            isCorrect,
          });
      setIsSaved(true);
    }
  }

  function saveAnswer() {
    debounceFnUpdateAnswer({ index, value: currentValue, isCorrect });
    setIsSaved(true);
  }

  useEffect(() => {
    setCurrentValue(value);
  }, [value]);

  return (
    <div className={inputClassName}>
      <input
        type="text"
        onChange={handleEdit}
        value={currentValue}
        placeholder={placeholder}
        className="input"
      />
      {!!max && (
        <span
          className={`${hintDefault} ${max - currentValue.length - 20 < 0 ? hintDanger : ""}`}
        >
          {max - currentValue.length - 20}
        </span>
      )}
      {!isSaved && hasSaveButton && currentValue.length > 0 && (
        <Button
          label="save"
          color="secondary"
          variant="outlined"
          onClick={saveAnswer}
        ></Button>
      )}
      {isSaved && isAnswer && currentValue.length > 0 && (
        <label className="container">
          <input
            type="checkbox"
            id="checkbox-input"
            checked={isCorrect}
            onChange={() => {
              setIsCorrect((isCorrect) => !isCorrect);
              debounceFnUpdateAnswer({
                index,
                value: currentValue,
                isCorrect: !isCorrect,
              });
            }}
          />
          <span className="checkmark"></span>
        </label>
      )}
    </div>
  );
}
