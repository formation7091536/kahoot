export interface Props {
  index?: number;

  value?: string;
  placeholder?: string;
  onChange?: (value) => void;

  max?: number;
  inputClassName?: string;
  hintDefault?: string;
  hintDanger?: string;

  isAnswer?: boolean;
  hasSaveButton?: boolean;
}
