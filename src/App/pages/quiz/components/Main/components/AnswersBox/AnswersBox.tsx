import { useEffect } from "react";
import { v4 as uuid4 } from "uuid";
import store, { useAppDispatch } from "../../../../../../shared/store";
import { createAnswer, updateAnswer } from "../../../../data/quizThunk";
import { Answer, Question } from "../../../../data/types";
import Input from "../Input";
import "./index.scss";

export default function AnswersBox({
  questionID,
  answers,
}: Readonly<{ questionID: string; answers: Answer[] | undefined }>) {
  const dispatch = useAppDispatch();

  useEffect(() => {
    for (let i = 0; i < 4; i++) {
      dispatch(
        createAnswer({
          id: uuid4(),
          title: "",
          isPhoto: false,
          question: questionID,
          isCorrect: false,
        }),
      );
    }
  }, [dispatch, questionID]);

  function saveUpdateAnswer({
    index,
    value,
    isCorrect,
  }: {
    index: number;
    value: string;
    isCorrect: boolean;
  }) {
    const id =
      store
        .getState()
        .quiz.quiz?.questions?.filter((q: Question) => questionID === q.id)[0]
        ?.answers?.[index]?.id ?? "";

    console.log(id);
    if (!id) return;

    dispatch(
      updateAnswer({
        id,
        title: value,
        isCorrect,
        isPhoto: false,
      }),
    );
  }

  return (
    <div className="answers-box">
      <Input
        index={0}
        max={95}
        placeholder="Add answer 1"
        inputClassName="answer red"
        hintDefault="chars-hint"
        hintDanger="danger-hint"
        isAnswer={true}
        value={answers?.[0]?.title ?? ""}
        onChange={saveUpdateAnswer}
      />
      <Input
        index={1}
        max={95}
        placeholder="Add answer 2"
        inputClassName="answer blue"
        hintDefault="chars-hint"
        hintDanger="danger-hint"
        isAnswer={true}
        value={answers?.[1]?.title ?? ""}
        onChange={saveUpdateAnswer}
      />
      <Input
        index={2}
        max={95}
        placeholder="Add answer 3"
        inputClassName="answer yellow"
        hintDefault="chars-hint"
        hintDanger="danger-hint"
        isAnswer={true}
        value={answers?.[2]?.title ?? ""}
        onChange={saveUpdateAnswer}
      />
      <Input
        index={3}
        max={95}
        placeholder="Add answer 4"
        inputClassName="answer green"
        hintDefault="chars-hint"
        hintDanger="danger-hint"
        isAnswer={true}
        value={answers?.[3]?.title ?? ""}
        onChange={saveUpdateAnswer}
      />
    </div>
  );
}
