import { useAppSelector } from "../../../../shared/store";
import AnswersBox from "./components/AnswersBox";
import QuestionInput from "./components/QuestionInput";
import "./index.scss";

export default function Main() {
  const questions = useAppSelector((store) => store.quiz.quiz?.questions);
  const question = questions?.filter((q) => q.isSelected)[0];

  console.log("from main (we want a re-render):", question);

  return (
    <main className="main">
      {!question ? (
        <h1>Start by adding a question!</h1>
      ) : (
        <>
          <QuestionInput question={question} />
          <AnswersBox questionID={question.id} answers={question.answers} />
        </>
      )}
    </main>
  );
}
