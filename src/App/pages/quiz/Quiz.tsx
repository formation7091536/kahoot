import { useAppDispatch, useAppSelector } from "../../shared/store";
import { me } from "../auth/data/authThunk";
import Header from "./components/Header";
import Main from "./components/Main";
import Navbar from "./components/Navbar";
import OptionsDrawer from "./components/OptionsDrawer";
import "./index.scss";

export default function Quiz() {
  const user = useAppSelector((store) => store.auth.user);
  const selectedQuestion = !!useAppSelector(
    (store) => store.quiz.quiz?.questions,
  )?.filter((q) => q.isSelected)[0];

  const dispatch = useAppDispatch();

  if (!user) dispatch(me());

  return (
    <div className="quiz-page">
      <Header />
      <Navbar />
      <Main />
      {selectedQuestion && <OptionsDrawer />}
    </div>
  );
}
