import { Navigate } from "react-router-dom";
import { useAppSelector } from "../../../shared/store";
import { getSavedQuiz } from "./savedQuiz";

interface Props {
  children: React.ReactNode;
}

const QuizGuard = ({ children }: Props) => {
  const quizId = useAppSelector(
    (state) => state.quiz.quiz?.id ?? getSavedQuiz(),
  );
  return !quizId ? <Navigate to="/dashboard" /> : children;
};

export default QuizGuard;
