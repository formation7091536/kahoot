export function saveQuiz(id: string) {
  localStorage.setItem("quiz", id);
}

export function getSavedQuiz() {
  return localStorage.getItem("quiz");
}
